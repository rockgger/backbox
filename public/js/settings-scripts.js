let currentStep = 1;

const settingsTitle = document.querySelector('.settings-title')
const settingsStepNumber = document.querySelector('#settings-step-number')
const step1 = document.querySelector('#step-1-div-body')
const step2 = document.querySelector('#step-2-div-body')
const step3 = document.querySelector('#step-3-div-body')
const btnBack = document.querySelector('.btn-back')
const btnContinue = document.querySelector('#btn-continue')
const btnCreateProject = document.querySelector('#btn-create-project')

const projectName = document.querySelector('#project-name')
const color = document.querySelector('#color')
const dbName = document.querySelector('#db-name')
const dbUser = document.querySelector('#db-user')
const dbPassword = document.querySelector('#db-password')
const webserviceKey = document.querySelector('#webservice-key')
const webserviceUid = document.querySelector('#webservice-uid')

let projectNameValue = ''
let colorValue = ''
let logotypeValue = ''
let dbNameValue = ''
let dbUserValue = ''
let dbPasswordValue = ''
let webserviceKeyValue = ''
let webserviceUidValue = ''

const colors = [
    { value: '#FFBA00' },
    { value: '#069800' },
    { value: '#37B3F4' },
    { value: '#0C1DA5' },
    { value: '#B337F4' },
    { value: '#F437D0' },
    { value: 'black' },
    { value: 'gray' },
]

let sliceStart = 0
let sliceEnd = 6

renderColors(colors, sliceStart, sliceEnd)

function renderStep() {
    if (currentStep === 1) {
        settingsTitle.innerHTML = 'Propiedades del proyecto'
        settingsStepNumber.innerHTML = 1
        step1.classList.remove('display-none')
        step1.classList.add('step-div-body')
        step2.classList.add('display-none')
        step2.classList.remove('step-div-body')
        step3.classList.add('display-none')
        step3.classList.remove('step-div-body')
        btnBack.classList.add('display-none')
        btnContinue.classList.remove('display-none')
        btnCreateProject.classList.add('display-none')
    }
    if (currentStep === 2) {
        settingsTitle.innerHTML = 'Configuración para conexión a la Base de datos'
        settingsStepNumber.innerHTML = 2
        step2.classList.remove('display-none')
        step2.classList.add('step-div-body')
        step1.classList.add('display-none')
        step1.classList.remove('step-div-body')
        step3.classList.add('display-none')
        btnBack.classList.remove('display-none')
        btnContinue.classList.remove('display-none')
        btnCreateProject.classList.add('display-none')
    }
    if (currentStep === 3) {
        settingsTitle.innerHTML = 'Verificación de la información'
        settingsStepNumber.innerHTML = 3
        step3.classList.remove('display-none')
        step1.classList.add('display-none')
        step1.classList.remove('step-div-body')
        step2.classList.add('display-none')
        step2.classList.remove('step-div-body')
        btnBack.classList.remove('display-none')
        btnContinue.classList.add('display-none')
        btnCreateProject.classList.remove('display-none')

        projectName.innerHTML = projectNameValue
        color.innerHTML = colorValue ?
            `<span 
        style="
          background-color: ${colorValue}; 
          border-radius: 6px; 
          width: 20px; 
          height: 20px; 
          display: inline-block;
          vertical-align: middle;"></span> <small class="color-707070">  Hexadecimal: ${colorValue}</small>` :
            '<span class="color-707070">(auto)</span>'
        dbName.innerHTML = dbNameValue.length > 0 ? dbNameValue : '<span class="color-707070">(auto)</span>'
        dbUser.innerHTML = dbUserValue.length > 0 ? dbUserValue : '<span class="color-707070">(auto)</span>'
        dbPassword.innerHTML = dbPasswordValue.length > 0 ? dbPasswordValue : '<span class="color-707070">(auto)</span>'
            /* webserviceKey.innerHTML = webserviceKeyValue
            webserviceUid.innerHTML = webserviceUidValue */
    }
}

function projectNameChange() {
    const txtProyectName = document.querySelector('[name="projectName"]')
    const step1RightDiv = document.querySelector('.step-1-right-div')
    const btnContinueDisabled = document.querySelector('#btn-continue-disabled')
    const btnContinue = document.querySelector('#btn-continue')

    projectNameValue = txtProyectName.value.trim()

    if (!projectNameValue) {
        btnContinueDisabled.classList.remove('display-none')
        btnContinue.classList.add('display-none')
        step1RightDiv.classList.add('step-1-blocker-div')
    } else {
        btnContinueDisabled.classList.add('display-none')
        btnContinue.classList.remove('display-none')
        step1RightDiv.classList.remove('step-1-blocker-div')
    }
}

function logoChange(e) {
    const inputFile = document.querySelector('#upload-file')
    const logo = (inputFile.value && inputFile.value.split('\\')) || ''
    const txtLogoFile = document.querySelector('.txt-logo-file')
    txtLogoFile.value = logo[logo.length - 1] || 'No se ha cargado el logotipo'
    logotypeValue = txtLogoFile.value


    const oFReader = new FileReader();
    oFReader.readAsDataURL(inputFile.files[0]);

    oFReader.onload = function(oFREvent) {
        document.querySelector('#logo-preview').src = oFREvent.target.result
    }
}

function nextStep() {
    currentStep++
    renderStep()
}

function prevStep() {
    currentStep--
    renderStep()
}


function renderColors(colorsArray, sliceStart, sliceEnd) {
    const colorsSliced = colorsArray.map((color) => `
    <div>
      <span 
        style="background-color: ${color.value}"
        onclick="selectColor(this)"
      ></span>
    </div>`).slice(sliceStart, sliceEnd)

    let colorsToRender = ''
    colorsSliced.map((color) =>
        colorsToRender += color
    )

    const body = document.querySelector('.color-picker')
    body.innerHTML = `
    <div onclick="prevColor()">
      <img width="9px" src="./assets/img/orange-left-arrow.svg" alt="flecha izquierda"/>
    </div>
    ${colorsToRender}
    <div onclick="nextColor()">
      <img width="9px" src="./assets/img/orange-right-arrow.svg" alt="flecha derecha"/>
    </div>`
}

function prevColor() {
    const colorsArray = colors
    colorsArray.unshift(colors[colors.length - 1])
    colorsArray.pop()
    renderColors(colorsArray, sliceStart, sliceEnd)
}

function nextColor() {
    const colorsArray = colors
    colorsArray.push(colors[0])
    colorsArray.shift()
    renderColors(colorsArray, sliceStart, sliceEnd)
}

function selectColor(colorElement) {
    const componentToHex = c => {
        let hex = Number(c).toString(16);
        return !hex.length ? "00" : hex.length === 1 ? "0" + hex : hex;
    }

    const rgbToHex = (r, g, b) => {
        return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    }

    if (colorElement.style.backgroundColor.includes('rgb')) {
        let rgb = colorElement.style.backgroundColor.match(/\d+/g)
        colorValue = rgbToHex(rgb[0] || '', rgb[1] || '', rgb[2] || '').toUpperCase()
    } else {
        colorValue = colorElement.style.backgroundColor.toUpperCase()
    }
    const color_input = document.querySelector('#color-input')
    color_input.value = colorValue;
}

function DBNameChange() {
    const txtDbName = document.querySelector('[name="dbName"]')
    dbNameValue = txtDbName.value && txtDbName.value.trim()
}

function AdminNameChange() {
    const txtAdminName = document.querySelector('[name="adminName"]')
    dbUserValue = txtAdminName.value && txtAdminName.value.trim()
}

function DBpasswordChange() {
    const txtDbPassword = document.querySelector('[name="dbPassword"]')
    dbPasswordValue = txtDbPassword.value && txtDbPassword.value
}

function createProject() {
    /* console.log(`  Nombre del proyecto: ${projectNameValue}
  Color de la secretaría asignado: ${colorValue}
  Logotipo: ${logotypeValue}
  Nombre de la base de datos: ${dbNameValue}
  Usuario de la base de datos: ${dbUserValue}
  Contraseña de la base de datos: ${dbPasswordValue}`)

    modal.style.display = 'block'; */
    const form = document.querySelector('#frm-create').submit()


}

const modal = document.getElementById('projectCreatedModal');

const closeSpan = document.getElementsByClassName('close')[0];

closeSpan.onclick = () => {
    modal.style.display = 'none';
}

window.onclick = event => {
    if (event.target === modal) {
        modal.style.display = 'none';
    }
}