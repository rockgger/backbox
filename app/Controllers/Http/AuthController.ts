
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class AuthController {



    public async login ({ auth, request, response, session  }: HttpContextContract) {

        
            const validationSchema = schema.create({
                email: schema.string({ trim: true }, [
                    rules.email()
                ]),
                password: schema.string({ trim: true }),
            })
        
            const userDetails = await request.validate({
                schema: validationSchema,
                messages: {
                    'email.required': 'Ingresa un correo',
                    'email.email': 'Ingresa un correo válido',
                    'password.required': 'Ingresa una contraseña',
                },
                cacheKey: request.url(),
            })

            const email = userDetails.email
            const password = userDetails.password
            try {
                await auth.attempt(email, password)
                response.redirect('/app')
            } catch (error) {
                session.flash('error', 'Usuario o contrasela invalidos.')
                session.flash('email', email)
                response.redirect('back')
            }
        
           

        


       
    }



    public async logout ({ auth, response }: HttpContextContract){
        await auth.logout()
        response.redirect('/login')
    }

}
