import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Application from '@ioc:Adonis/Core/Application'
import Project from 'App/Models/Project'
import Database from '@ioc:Adonis/Lucid/Database'
//import Route from '@ioc:Adonis/Core/Route'

export default class AppsController {


    //vista de /
    public async start ({ response }: HttpContextContract) {

        
        response.redirect(`/login`)
    }

    //vista de login
    public async login ({ view, auth, response }: HttpContextContract) {

        if (await auth.check()) {
            response.redirect(`/app`)
          }
        
        return view.render('app/login')
    }

    //vista de inicio de la app
    public async index ({ view,auth }: HttpContextContract) {
        
        const user =  auth.user
        
        return view.render('app/index',{user})
    }


    //vista de crear proyecto base (BOX)
    public async create ({ view, auth }: HttpContextContract) {


        /* const menu = false; */
        const modules = [
            'módulo1',
            'módulo2',
            'módulo3',
            'módulo4',
            'módulo5',
            'módulo6',
            'módulo7',
            'módulo8',
            'módulo9',
            'módulo10',
        ];

        const user =  auth.user
        return view.render('app/create', {modules,user})
    }


    //Gardar proyecto base (BOX)
    public async store ({ request, response, session   }: HttpContextContract) {

        const name = request.input('projectName')
        let color = request.input('color')
        const logo = request.file('logo')
        let db_name = request.input('dbName')
        let db_user = request.input('adminName')
        let db_password = request.input('dbPassword')
        let url_logo;
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        
        //validaciones
        if(!name){
            session.flash('error', 'Ingresa un nombre para el proyecto')
            response.redirect('back')
        }

        if(!logo){
            url_logo = '/images/logo_demo.png';
        }else{
            let name_logo = `${new Date().getTime()}.${logo.extname}`
            url_logo = `/images/logos/${name_logo}`
            await logo.move(Application.publicPath('images/logos'), {
                name: `${name_logo}`,
            })
        }

        if(!db_name){
            let ch = name;
            ch = ch.replace(/[*~^=&%$#{}+<>°|¨¬()"!¡'¿ñ´@/.:,;ª©₡€½?áéíóú]/gi, "")
            ch = ch.replace(/ /g, "_")
            db_name = ch.toLowerCase()
        }
        
        if(!db_user){
            db_user = 'user_'+db_name.toLowerCase()
        }

        if(!db_password){
            for (let i = 0; i < 15; i++) {
                db_password += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
        }

        //generamos UID y KEY
        let uid_temp = name
        let pr
        do{
            uid_temp = uid_temp.replace(/[*~^=&%$#{}+<>°|¨¬()"!¡'¿ñ´@/.:,;ª©₡€½?áéíóú]/gi, "")
            uid_temp = uid_temp.replace(/ /g, "")
            pr = await Project.findBy('uid', uid_temp)
        }while(pr)

        const uid = uid_temp.toLowerCase()
        let key = '';

        for (let i = 0; i < 45; i++) {
            key += characters.charAt(Math.floor(Math.random() * charactersLength));
        }


        const project = new Project()
        project.name = name
        project.color = color
        project.logo = url_logo
        project.db_name = db_name
        project.db_user = db_user
        project.db_password = db_password
        project.uid = uid
        project.key = key
        await project.save()

        //creamos una carpeta temporal para el proyecto
        const fs = require('fs');
        let bs = `dockerfiles/project_${uid}`;
        
        //docker que se copia al WS
        let port= "8080";
        let dk = `#Docker V0.0.1' \n`+
        `#Building app for production \n`+
        `#Se utiliza para generar la imagen de producción (build) \n`+
        `FROM node:14 \n`+
        ` \n`+
        `RUN apt-get update && apt-get install -y git \n`+
        `COPY . /app \n`+
        `WORKDIR /app \n`+
        `RUN npm install \n`+
        `RUN node ace build --production \n`+
        `RUN cp .env.example build/.env \n`+
        `WORKDIR /app/build \n`+
        `RUN npm ci --production \n`+
        `RUN node ace generate:key \n`+
        ` \n`+
        `#variables de entorno \n`+
        `ENV NODE_ENV=production \n`+
        `ENV DB_CONNECTION=pg \n`+
        `ENV PG_HOST=127.0.0.1 \n`+
        `ENV PG_PORT=5432 \n`+
        `ENV PG_USER=${db_user} \n`+
        `ENV PG_PASSWORD=${db_password} \n`+
        `ENV PG_DB_NAME=${db_name} \n`+
        `ENV WS_KEY=${key} \n`+
        `ENV WS_UID=${uid} \n`+
        `ENV APP_TITLE="${name}" \n`+
        ` \n`+
        `EXPOSE  ${port} \n`+
        `CMD ["node", "server.js"]`;

        //dockerpara construir la imagen del webservice
        let dkbuild = `#Docker V0.0.1' \n`+
        `#Building app for production \n`+
        `#Se utiliza para generar la imagen de producción (build) \n`+
        `FROM node:14 \n`+
        `#variables de entorno \n`+
        ` \n`+
        `RUN apt-get update && apt-get install -y git \n`+
        `RUN mkdir /app \n`+
        `RUN cd /app && git clone https://rockgger@bitbucket.org/rockgger/webservice.git \n`+
        `WORKDIR /app/webservice \n`+
        `COPY public/${bs}/Dockerfile /app/webservice \n`+ //al realizar el docker run se debe crear el volumen - /dockers:/app/build/public/dockerfiles        
        `RUN npm install \n`+
        `RUN node ace build \n`+
        `RUN cp .env.example build/.env \n`+
        `RUN cp package.json build/package.json \n`+
        `RUN cp package-lock.json build/package-lock.json \n`+
        `WORKDIR /app/webservice/build \n`+
        `RUN npm ci --production \n`+
        `RUN node ace generate:key \n`+
        ` \n`+
        `ENV NODE_ENV=production \n`+
        `ENV DB_CONNECTION=pg \n`+
        `ENV PG_HOST=127.0.0.1 \n`+
        `ENV PG_PORT=5432 \n`+
        `ENV PG_USER=${db_user} \n`+
        `ENV PG_PASSWORD=${db_password} \n`+
        `ENV PG_DB_NAME=${db_name} \n`+
        `ENV WS_KEY=${key} \n`+
        `ENV WS_UID=${uid} \n`+
        `ENV APP_TITLE="${name}" \n`+
        ` \n`+
        `EXPOSE  ${port} \n`+
        `CMD ["node", "server.js"]`;

        //dockerpara construir la imagen del frontend
        let dk_front_build = `FROM node:14 \n`+
        `RUN apt-get update && apt-get install -y git \n`+
        `RUN mkdir /app \n`+
        `#clonar front plantilla \n`+
        `RUN cd /app && git clone https://rockgger@bitbucket.org/rockgger/frontbox.git \n`+
        `WORKDIR /app/frontbox \n`+
        `COPY public/${bs}/front/Dockerfile /app/frontbox \n`+
        `RUN cp .env.example .env \n`+
        `RUN npm install \n`+
        `\n`+
        `#variables de entorno \n`+
        `ENV REACT_APP_WS_KEY=${key} \n`+
        `ENV REACT_APP_WS_UID=${uid} \n`+
        `ENV REACT_APP_TITLE="${name}" \n`+ 
        `ENV REACT_APP_COLOR=${color} \n`+ 
        `ENV REACT_APP_WS_URL=http://localhost:80 \n`+ 
        `ENV REACT_APP_VAPI=v1 \n`+
        `EXPOSE  3000 \n`+
        `CMD ["npm", "start"]`;

        //docker que se agrega en la imagen de la plantilla frontend
        let dk_front= `#Docker V0.0.1' \n`+
        `#Building app for production \n`+
        `#Se utiliza para generar la imagen de producción (build) \n`+
        `FROM node:14 \n`+
        ` \n`+
        `RUN apt-get update && apt-get install -y git \n`+
        `COPY . /app \n`+
        `WORKDIR /app \n`+
        `RUN npm install \n`+
        ` \n`+
        `#variables de entorno \n`+
        `ENV REACT_APP_WS_KEY=${key} \n`+
        `ENV REACT_APP_WS_UID=${uid} \n`+
        `ENV REACT_APP_TITLE="${name}" \n`+ 
        `ENV REACT_APP_COLOR=${color} \n`+ 
        `ENV REACT_APP_WS_URL=http://localhost:80 \n`+ 
        `ENV REACT_APP_VAPI=v1 \n`+
        ` \n`+
        `EXPOSE  3000 \n`+
        `CMD ["npm", "start"]`;

        //crear BD
        const createDatabase = `CREATE DATABASE ${db_name};`;    
        const db_create = await Database.rawQuery(createDatabase);

        //crear usuario de BD
        const createUser = `CREATE USER ${db_user} WITH PASSWORD '${db_password}'`;
        await Database.rawQuery(createUser);

        //privilegios para el usuario
        const grantPermissions = `GRANT ALL PRIVILEGES ON DATABASE ${db_name} TO ${db_user}`;
        await Database.rawQuery(grantPermissions);

        console.log(db_create);
        //crear directorios temporales donde se almacenan los dockers del proyecto
        await fs.mkdir(Application.publicPath(`${bs}`), { recursive: true }, (err) => {
            console.log("creando directorio...");
            if (err) throw err;
            console.log('Directorio creado');

            fs.mkdir(Application.publicPath(`${bs}/front`), { recursive: true }, (err) => {
                console.log("creando directorio front...");
                if (err) throw err;
                console.log('Directorio creado front');
                //crear docker dek front
                fs.writeFile(Application.publicPath(`${bs}/front/Dockerfile`), dk_front, err => {
                    console.log("creando front Dockerfile...");
                    if (err) throw err
                    console.log('Dockerfile front creado');
                    fs.writeFile(Application.publicPath(`${bs}/front/Dockerfilebuild`), dk_front_build, err => {
                        console.log("creando front Dockerfilebuild...");
                        if (err) throw err
                        console.log('Dockerfilebuild front creado');
                    });
                });
            });
            //crear docker del back
            fs.writeFile(Application.publicPath(`${bs}/Dockerfile`), dk, err => {
                console.log("creando Dockerfile...");
                if (err) throw err;
                console.log('Dockerfile creado');

                fs.writeFile(Application.publicPath(`${bs}/Dockerfilebuild`), dkbuild, err => {
                    console.log("creando Dockerfilebuild...");
                    if (err) throw err
                    console.log('Dockerfilebuild creado');
                });
            });
            
        });

        //ejecutamos script az con bash para crear imagen y subirla a ACR
        //la construcción inicial puede tardar de 3 a 5 min, mirar la consola de tareas de ACR
        const { exec } = require('child_process');
        let az_user = "admindevops@medellin.gov.co";
        let az_pass = "Medellin2021//*";
        //let acr = "demo1box";
        let acr = `Alcaldia${uid}`;
        let group = "Alcaldia";
        //let suscription = "Suscripcion2";
        let suscription = "f43314b2-7c48-4c4d-820d-4bb29f3c4878";// id "Alcaldía de Medellín (Secretaria de Innovacion Digital)";
        console.log('Ejecutando CLI de AZC');
        //Ejecutando script de azure para generar la imagen y subir todo al registro de contenedores de azure
        exec(`az login -u ${az_user} -p ${az_pass}`, (error, stdout) => {
            if (error) {
                console.error(`error: ${error.message}`);
                return;
            }
            console.log(`login stdout:\n${stdout}`);

            //crear container para el proyecto
            exec(`az acr create --resource-group ${group} --name ${acr} --sku Basic --admin-enabled true --subscription ${suscription}`, (error, stdout) => {
                if (error) {
                    console.error(`error: ${error.message}`);
                    return;
                }
                console.log(`resource stdout:\n${stdout}`);

                exec(`az acr build --image webservice_${uid}:init --registry ${acr} --subscription ${suscription} --file public/${bs}/Dockerfilebuild .`, (error, stdout) => {
                    if (error) {
                        console.error(`error: ${error.message}`);
                        return;
                    }
                    console.log(`build stdout:\n${stdout}`);
                });

                exec(`az acr build --image frontend_${uid}:init --registry ${acr} --subscription ${suscription} --file public/${bs}/front/Dockerfilebuild .`, (error, stdout) => {
                    if (error) {
                        console.error(`error: ${error.message}`);
                        return;
                    }
                    console.log(`build stdout:\n${stdout}`);
                });
            });

            
        });

        
        
         response.redirect(`/show/${project.id}`)
        
        //return {name, color, url_logo, db_name, db_user, db_password, uid, key, dockernode, dockerserver, confserver, dkcompose}
    }


    //vista de ver proyecto base (BOX)
    public async show ({ params, view, auth }: HttpContextContract) {
        const user =  auth.user
        const project = await Project.findOrFail(params.project_id)

        let bs = `dockerfiles/project_${project.uid}`;
        let dockernode = `/docker/Dockerfile`
        //let dockerserver = `/docker/Dockerfileserver`
        //let confserver = `/docker/nginx.conf`
        let dkcompose = `/${bs}/docker-compose.yml`
        let dkzip = '#';

        /* const fs = require('fs');
        const archiver = require('archiver');

        let output = fs.createWriteStream(Application.publicPath(`${bs}/resources.zip`));
        let archive = archiver('zip');

        output.on('close', function() {
            console.log(archive.pointer() + ' total bytes');
            console.log('archiver has been finalized and the output file descriptor has closed.');
        });
        output.on('end', function() {
            console.log('Data has been drained');
            
        })
        archive.pipe(output);
        //archive.directory(Application.publicPath('docker'), false);
        archive.file(Application.publicPath(`${bs}/Dockerfile`), {name: 'Dockerfile'});
        archive.file(Application.publicPath(`${bs}/docker-compose.yml`), {name: 'docker-compose.yml'});
        archive.finalize();
        dkzip = `/${bs}/resources.zip`; */
        //return params.project_id;
        return view.render('app/show', {project, dockernode, dkcompose, dkzip, user})
    }

    //vista lista de proyectos
    public async list ({  view, auth }: HttpContextContract) {
        const user =  auth.user
        const projects = await Project.all()

        
        return view.render('app/list', {projects,user})
    }
    
}
