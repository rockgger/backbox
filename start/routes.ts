/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes/index.ts` as follows
|
| import './cart'
| import './customer'
|
*/

import HealthCheck from '@ioc:Adonis/Core/HealthCheck'
import Route from '@ioc:Adonis/Core/Route'

//Vista inicial
Route.get('/login', 'AppsController.login').as('login')
Route.post('/login', 'AuthController.login').as('login.auth')

Route.get('/logout', 'AuthController.logout').as('logout')

//Vista inicial
Route.get('/', 'AppsController.start').as('start')

//Vista inicial
Route.get('/app', 'AppsController.index').as('index').middleware('auth')

//crear proyecto base
Route.get('/create', 'AppsController.create').as('app.create').middleware('auth')

//guardar proyecto base
Route.post('/store', 'AppsController.store').as('app.store').middleware('auth')

//resultado
Route.get('/show/:project_id?', 'AppsController.show').as('app.show').middleware('auth')

//Vista listado de proyectos
Route.get('/list', 'AppsController.list').as('app.list').middleware('auth')



//verifica conexión a la BD habilitar la healthCheck dentro del archivo config/database.ts
Route.get('/health', async ({ response }) => {
    const report = await HealthCheck.getReport()
    
    return report.healthy
      ? response.ok(report)
      : response.badRequest(report)
  }).middleware('auth')


