#Docker V0.0.2
#Building app for production
FROM node:14
RUN apt-get update && apt-get install -y \
	git

COPY . /app
WORKDIR /app
RUN npm install
RUN node ace build --production
RUN cp .env.example build/.env
WORKDIR /app/build
RUN npm ci --production
RUN mkdir /app/build/tmp
#VOLUME /app/build/tmp
#al realizar el docker run se debe crear el volumen - /dockers:/app/build/public/dockerfiles
#VOLUME /dockers /app/build/public/dockerfiles
RUN node ace migration:run
RUN node ace db:seed
RUN curl -sL https://aka.ms/InstallAzureCLIDeb | bash
EXPOSE  3333

CMD ["node", "server.js"]